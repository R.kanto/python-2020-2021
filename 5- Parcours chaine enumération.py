# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 14:38:25 2020

@author: Ąřmāțəŕąşū

Tp1 - Exercice 5 : Parcous dans une chaine, utilisation dictionnaire
"""
chaine = str(input("Chaine de caractère: "))

dico = {}

for i in chaine:
    if i == " ":
        continue
    if (dico.get(i) is None):
        dico[i] = 1
    else:
        dico[i] += 1

for i in dico:
    print(str(i) + ": " + str(dico[i]))
