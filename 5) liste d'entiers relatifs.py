# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 11:33:49 2020

@author: Ąřmāțəŕąşū

Tp2 - Exercice 5 : calculs sur les listes d'entiers relatifs
"""

def cmptNegatifs(liste):
    nbNeg = 0
    for i in range(0, len(liste)):
        if liste[i] < 0:
            nbNeg += 1
    
    return nbNeg;

def initialise():

    liste = []
    
    while 1:
        v = str(input("Valeur: "))

        if v == "":
            print("Programme terminée")
            break;
        
        liste.append(int(v))
    
    return liste


print("Nombre de nombre négatif: " + str(cmptNegatifs(initialise())))
