# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 11:19:57 2020

@author: Ąřmāțəŕąşū

Tp2 - Exercice 4 : factorielle
"""
x = int(input("Entrez un entier :"))

def factorielle(x : int):
    if x<2:
        return 1
    else: 
        return x*factorielle(x-1)
print(factorielle(x))