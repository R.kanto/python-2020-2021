# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 14:10:53 2020

@author: Ąřmāțəŕąşū

Tp1 - Exercice 3 : Convertisseur d'unité angulaire'
"""
import math

n = input("Entrez une valeur entre [0;2pi[ : ")
n=float(n)
if 0<=n<=(2*math.pi):
    x = input("Entrez g pour grade et d pour degre :")
    x = str(x)
    if x == 'g':
        a = n * math.pi/200
        a = float(a)
        print("Un angle de ",n," radians vaut ",a," grades")
    elif x == 'd':
        a = n * 180/math.pi
        a = float(a)
        print("Un angle de ",n," radians vaut ",a," degres")
else:
    print("Erreur! Veuillez entrer une autre valeur...")