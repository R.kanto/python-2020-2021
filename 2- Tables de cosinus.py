# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 13:55:56 2020

@author: Ąřmāțəŕąşū

Tp1 - Exercice 2 : Tables de cosinus
"""
import math

n = input("Entrez une valeur : ")
i,a = 0,0
n,i,a = float(n), int(i), float(a)

if 0>n>360:
    print("Erreur! Entrez nombre entre [0;360[")
else:
    while i <= n:
        a = math.cos(i)
        print("cos ",i,"° = ",a)
        i = i +10