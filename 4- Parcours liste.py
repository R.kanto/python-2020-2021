# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 14:27:15 2020

@author: Ąřmāțəŕąşū

Tp1 - Exercice 4 : Parcours dans une liste
"""
l1 = [9,-12,-32,7,-1,4,-8,0,-99,0]
l2 = []

for x in l1:
    if x >= 0:
        l2.append(x)
print(l2)        

